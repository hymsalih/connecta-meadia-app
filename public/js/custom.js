jQuery(document).ready(function() {
    //textillate
    jQuery('.tlt').textillate();

    //wow
    new WOW().init();

    //choose layout
    jQuery('input[type=radio][name=layout]').change(function() {
        jQuery('.side_nav').removeClass('d-none');
    });
    jQuery('.side_nav_action a.btn-cancel').click(function(e) {
        e.preventDefault();
        jQuery('.side_nav').addClass('d-none');
    });

    //publish layout
    jQuery('button.btn-publish').click(function() {
        jQuery('.side_nav').removeClass('d-none');
    });

    //entrance
    jQuery('.entrance').click(function(e) {
        e.preventDefault();
        jQuery(this).siblings('.entrance-block').removeClass('d-none');
    });
    jQuery('.entrance-block .btn-cancel').click(function(e) {
        e.preventDefault();
        jQuery('.entrance-block').addClass('d-none');
    });


    //delete notification
    jQuery('.btn-running-delete').click(function(e) {
        e.preventDefault();
        jQuery('.notification-running').removeClass('d-none');
    });
    jQuery('.btn-scheduled-delete').click(function(e) {
        e.preventDefault();
        jQuery('.notification-scheduled').removeClass('d-none');
    });
    jQuery('.delete-running').click(function(e) {
        e.preventDefault();
        jQuery('.notification-running').addClass('d-none');
    });
    jQuery('.delete-scheduled').click(function(e) {
        e.preventDefault();
        jQuery('.notification-scheduled').addClass('d-none');
    });
});
