@extends('layouts.app')
@section('css')
    <style>
    </style>
@endsection

@section('content')


<a href="#" class="help"><i class="far fa-question-circle"></i></a>
<div class="container-fluid p-0">

    <section class="p-3 p-lg-5 d-flex align-items-center" >
        <div class="w-100 text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <img src="img/potw.jpg" alt="" class="w-100 wow fadeIn" data-wow-duration="2s">
                    </div>
                </div>
                <div class="w-100 py-3"></div>
                <div class="row">
                    <div class="col-md-4 pt-2">
                        <div class="box wow fadeInLeft" data-wow-delay="1s" data-wow-duration="2s">
                            <a href="{{ route('template_create') }}"><img src="img/potw.jpg" alt="" class="img-fluid"></a>
                            <p class="mt-3">Product of the week</p>
                        </div>

                    </div>
                    <div class="col-md-4 pt-2">
                        <div class="box wow fadeInUp" data-wow-delay="1.5s" data-wow-duration="2s">
                            <a href="{{ route('template_create') }}"><img src="img/potw.jpg" alt="" class="img-fluid"></a>
                            <p class="mt-3">Recipe Promotion</p>
                        </div>

                    </div>
                    <div class="col-md-4 pt-2">
                        <div class="box wow fadeInRight" data-wow-delay="2s" data-wow-duration="2s">
                            <a href="{{ route('template_create') }}"><img src="img/potw.jpg" alt="" class="img-fluid"></a>
                            <p class="mt-3">Cashiers Promotion</p>
                        </div>

                    </div>
                </div>
                <div class="text-center mt-5 wow fadeIn" data-wow-delay="3.5s">
                    <a href="{{ route('publish') }}" class="btn btn-primary"><i class="fas fa-arrow-left"></i> Back</a>
                </div>
            </div>
        </div>
    </section>

</div>
@endsection
