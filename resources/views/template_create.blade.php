@extends('layouts.app')
@section('css')
    <style>
    </style>
@endsection

@section('content')

    <!-- NOTIFICATION START -->
    <div class="notification d-none">
        <div class="notification_content">
            <h3>
                Your Layout has been published!
                <br>
                <a href="publish.html"><i class="fas fa-check-circle"></i></a>
            </h3>
        </div>
    </div>
    <!-- NOTIFICATION END -->

    <!-- SIDEBAR START -->
    <div class="side_nav d-none">
        <h5>Choose a Screen</h5>
        <ul class="screen">
            <li>
                <a href="#" id="entrance" class="d-flex">
                    <i class="fa fa-desktop" aria-hidden="true"></i>
                    <span><b>Entrance</b><br>Landscape 50"</span>
                </a>

                <div class="d-none" id="entrance-block">
                    <table class="pow">
                        <tr>
                            <td><img src="img/potw.jpg" alt="" class="img-fluid"></td>
                            <td>Product of the week</td>
                        </tr>
                    </table>

                    <hr/>

                    <p><i class="far fa-calendar-alt"></i> Start Date</p>

                    <div id="v-cal">
                        <div class="vcal-header">
                            <button class="vcal-btn" data-calendar-toggle="previous">
                                <svg height="24" version="1.1" viewbox="0 0 24 24" width="24"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M20,11V13H8L13.5,18.5L12.08,19.92L4.16,12L12.08,4.08L13.5,5.5L8,11H20Z"></path>
                                </svg>
                            </button>

                            <div class="vcal-header__label" data-calendar-label="month">
                                March 2017
                            </div>


                            <button class="vcal-btn" data-calendar-toggle="next">
                                <svg height="24" version="1.1" viewbox="0 0 24 24" width="24"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M4,11V13H16L10.5,18.5L11.92,19.92L19.84,12L11.92,4.08L10.5,5.5L16,11H4Z"></path>
                                </svg>
                            </button>
                        </div>
                        <div class="vcal-week">
                            <span>Mon</span>
                            <span>Tue</span>
                            <span>Wed</span>
                            <span>Thu</span>
                            <span>Fri</span>
                            <span>Sat</span>
                            <span>Sun</span>
                        </div>
                        <div class="vcal-body" data-calendar-area="month"></div>
                    </div>

                    <div class="pt-2"><input type="checkbox"> Constantly show</div>

                    <hr/>

                    <p><i class="far fa-calendar-alt"></i> Weekly</p>

                    <div class="d-flex justify-content-around text-center">
                        <span>Mon <br> <input type="checkbox"></span>
                        <span>Tue <br> <input type="checkbox"></span>
                        <span>Wed <br> <input type="checkbox"></span>
                        <span>Thu <br> <input type="checkbox"></span>
                        <span>Fri <br> <input type="checkbox"></span>
                        <span>Sat <br> <input type="checkbox"></span>
                        <span>Sun <br> <input type="checkbox"></span>
                    </div>
                    <div class="pl-2 pt-2"><input type="checkbox"> Whole Week</div>

                    <hr/>

                    <p><i class="far fa-clock"></i> Time</p>
                    <div class="pb-2">Show Between</div>
                    <div class="d-flex justify-content-around text-center">
                        <input type="text" placeholder="00:00:00" style="width: 100px;" class="timepicker">
                        <div class="p-2"></div>
                        <input type="text" placeholder="00:00:00" style="width: 100px;" class="timepicker">
                    </div>
                    <div class="pt-2"><input type="checkbox"> Whole Day</div>
                    <div class="text-center pt-4 d-flex justify-content-around">
                        <a href="index.html" class="btn btn-primary btn-vsm">Menu</a>
                        <a href="#" class="btn btn-primary btn-vsm btn-cancel">Back</a>
                        <a href="#" class="btn btn-primary btn-vsm btn-save">Save</a>
                    </div>
                </div>
            </li>
            <li>
                <a href="#" class="d-flex">
                    <i class="fa fa-desktop" aria-hidden="true"></i>
                    <span><b>Bread</b><br>Landscape 50"</span>
                </a>
            </li>
            <li>
                <a href="#" class="d-flex">
                    <i class="fa fa-desktop" aria-hidden="true"></i>
                    <span><b>Meats</b><br>Landscape 50"</span>
                </a>
            </li>
            <li>
                <a href="#" class="d-flex">
                    <i class="fa fa-desktop" aria-hidden="true"></i>
                    <span><b>Dairies</b><br>Landscape 50"</span>
                </a>
            </li>
            <li>
                <a href="#" class="d-flex">
                    <i class="fa fa-desktop" aria-hidden="true"></i>
                    <span><b>Cashier</b><br>Landscape 50"</span>
                </a>
            </li>
        </ul>
        <p class="side_nav_action">
            <a href="#" class="btn btn-primary btn-cancel">Cancel</a>
        </p>
    </div>
    <!-- SIDEBAR END -->

    <a href="#" class="help"><i class="far fa-question-circle"></i></a>
    <div class="container-fluid p-0">

        <section class="p-3 p-lg-5 d-flex align-items-center">
            <div class="w-100 text-center">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3">
                            <input type="text" class="form-control" placeholder="Layout Name">
                            <div class="text-right pt-3">
                                <button class="btn btn-primary btn-publish">Publish</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a href="choose_screen.html"><img src="img/potw.jpg" alt="" class="w-100"></a>

                            <div class="input-group mt-3" style="background: #b37f06;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">A</span>
                                </div>
                                <a href="media.html" class="btn btn-light m-1 btn-sm">CHOOSE FILE</a>
                            </div>
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">B</span>
                                </div>
                                <input type="text" placeholder="Product name" class="form-control" aria-label="Default"
                                       aria-describedby="inputGroup-sizing-default">
                            </div>
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">C</span>
                                </div>
                                <input type="text" placeholder="Product description" class="form-control"
                                       aria-label="Default" aria-describedby="inputGroup-sizing-default">
                            </div>
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">D</span>
                                </div>
                                <input type="text" placeholder="Product informartion" class="form-control"
                                       aria-label="Default" aria-describedby="inputGroup-sizing-default">
                            </div>
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default">D</span>
                                </div>
                                <input type="text" placeholder="Product price" class="form-control" aria-label="Default"
                                       aria-describedby="inputGroup-sizing-default">
                            </div>
                        </div>
                    </div>

                    <div class="text-center mt-5">
                        <a href="templates.html" class="btn btn-primary"><i class="fas fa-arrow-left"></i> Back</a>
                    </div>
                </div>
            </div>
        </section>


    </div>
@endsection
