@extends('layouts.app')

@section('content')

    <!-- SIDEBAR START -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-purple fixed-top d-none" id="sideNav">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="publish.html"><i class="fas fa-cloud-upload-alt"></i>
                        Publish</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="screens.html"><i class="fa fa-desktop"
                                                                                 aria-hidden="true"></i> Screens</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="shared.html"><i class="far fa-image"></i> Shared
                        Media</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- SIDEBAR END -->

    <a href="#" class="help"><i class="far fa-question-circle"></i></a>

    <div class="container-fluid p-0">
        <section class="mh-100 p-3 p-lg-5 d-flex align-items-center">
            <div class="w-100 text-center">
                <h2 class="pb-5 text-primary tlt" data-in-effect="swing">Publish</h2>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 offset-md-1">
                            <div class="card wow fadeInLeft" data-wow-delay="1s">
                                <div class="card-body">
                                    <h5 class="card-title text-primary">Publish Existing Layout</h5>
                                    <p class="card-text">With supporting text below as a natural lead-in to additional
                                        content.</p>
                                    <a href="{{ route('layouts') }}" class="btn btn-primary">Go!</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4">
                            <div class="card wow fadeInRight" data-wow-delay="2s">
                                <div class="card-body">
                                    <h5 class="card-title text-primary">Create A New Layout</h5>
                                    <p class="card-text">With supporting text below as a natural lead-in to additional
                                        content.</p>
                                    <a href="{{ route('templates') }}" class="btn btn-primary">Go!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-5 wow fadeIn" data-wow-delay="3s">
                        <a href="{{ route('home') }}" class="btn btn-primary"><i class="fas fa-arrow-left"></i> Menu</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
