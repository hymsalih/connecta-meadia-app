@extends('layouts.app')
@section('css')
    <style>
    </style>
@endsection

@section('content')

    <a href="#" class="help"><i class="far fa-question-circle"></i></a>
    <div class="container-fluid p-0">
        <section class="headerSec media">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-4">
                        <a href="{{ route('home') }}" class="btn btn-primary"><i class="fas fa-arrow-left"></i> Back</a>
                    </div>
                    <div class="col-md-4">
                        <h2 class="tlt" data-in-effect="swing">Media</h2>
                    </div>
                    <div class="col-md-4 d-flex">
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01">
                                <label class="custom-file-label" for="inputGroupFile01">Upload file</label>
                            </div>
                        </div>
                        <div class="p-1"></div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-default"><i
                                        class="fas fa-search"></i></span>
                            </div>
                            <input type="text" placeholder="Search file" class="form-control" aria-label="Default"
                                   aria-describedby="inputGroup-sizing-default">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="p-3 p-lg-5 d-flex align-items-center">
            <div class="w-100">
                <div class="container product">
                    <div class="row">
                        <div class="col-md-2 col-sm-6 pt-2">
                            <img src="img/product.jpg" alt="" class="img-fluid">
                            <input type="radio" value="None" id="product_1" name="check">
                            <label for="product_1"></label>
                            <p>Product of the week</p>
                        </div>
                        <div class="col-md-2 col-sm-6 pt-2">
                            <img src="img/product.jpg" alt="" class="img-fluid">
                            <input type="radio" value="None" id="product_2" name="check">
                            <label for="product_2"></label>
                            <p>Product of the week</p>
                        </div>
                        <div class="col-md-2 col-sm-6 pt-2">
                            <img src="img/product.jpg" alt="" class="img-fluid">
                            <input type="radio" value="None" id="product_3" name="check">
                            <label for="product_3"></label>
                            <p>Product of the week</p>
                        </div>
                        <div class="col-md-2 col-sm-6 pt-2">
                            <img src="img/product.jpg" alt="" class="img-fluid">
                            <input type="radio" value="None" id="product_4" name="check">
                            <label for="product_4"></label>
                            <p>Product of the week</p>
                        </div>
                        <div class="col-md-2 col-sm-6 pt-2">
                            <img src="img/product.jpg" alt="" class="img-fluid">
                            <input type="radio" value="None" id="product_5" name="check">
                            <label for="product_5"></label>
                            <p>Product of the week</p>
                        </div>
                        <div class="col-md-2 col-sm-6 pt-2">
                            <img src="img/product.jpg" alt="" class="img-fluid">
                            <input type="radio" value="None" id="product_6" name="check">
                            <label for="product_6"></label>
                            <p>Product of the week</p>
                        </div>
                        <div class="w-100 py-3"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
