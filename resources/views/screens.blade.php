@extends('layouts.app')
@section('css')
    <style>
        body {
            padding-top: 0;
            padding-right: 17rem;
        }
    </style>
@endsection

@section('content')

    <!-- SIDEBAR START -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-purple fixed-top" id="sideNav">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                @foreach($resolutions as $resolution)
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ route('screen', $resolution->resolutionId) }}"><i
                                class="fa fa-desktop"></i> {{ $resolution->resolution }}
                            <br><span>{{ $resolution->width }} × {{ $resolution->height }}</span></a>
                    </li>
                @endforeach
            </ul>

        </div>
        <p class="side_nav_action">
            <a href="{{ route('home') }}" class="btn btn-primary">Back <i class="fas fa-arrow-right"></i></a>
        </p>
    </nav>
    <!-- SIDEBAR END -->

    <a href="#" class="help"><i class="far fa-question-circle"></i></a>

    <div class="container-fluid p-0">
        <section class="resume-section p-3 p-lg-5 d-flex align-items-center">
            <div class="w-100 text-center">
                <h2 class="pb-5 text-primary tlt" data-in-effect="swing">Screens</h2>
            </div>
        </section>
    </div>
@endsection
