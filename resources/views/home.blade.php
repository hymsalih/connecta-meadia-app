@extends('layouts.app')
@section('css')
    <style>
        body {
            padding-top: 0;
            padding-right: 17rem;
        }
    </style>
@endsection

@section('content')

    <!-- SIDEBAR START -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-purple fixed-top" id="sideNav">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{ route('publish') }}"><i
                            class="fas fa-cloud-upload-alt"></i> Publish</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{ route('screens') }}"><i class="fa fa-desktop"
                                                                                           aria-hidden="true"></i>
                        Screens</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="{{ route('shared_media') }}"><i
                            class="far fa-image"></i> Shared Media</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- SIDEBAR END -->

    <a href="#" class="help"><i class="far fa-question-circle"></i></a>

    <div class="container-fluid p-0">
        <section class="resume-section p-3 p-lg-5 d-flex align-items-center">
            <div class="w-100 text-center">
                <h2 class="pb-5 text-primary tlt" data-in-effect="rollIn">Welcome to your Screen Manager</h2>
                <p class="pt-3 wow bounceInUp" data-wow-delay="2.5s" data-wow-duration="2s"><img
                        src="{{ asset('img/sl1.jpg') }}" alt=""></p>
            </div>
        </section>
    </div>
@endsection
