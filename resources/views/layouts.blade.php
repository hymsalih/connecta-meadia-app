@extends('layouts.app')

@section('css')
    {{--    Clock styles for this template  --}}
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css">
    {{--    <link href="{{ asset('css/vanillaCalendar.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/pg-calendar/css/pignose.calendar.min.css') }}"/>
    <style>
        .pignose-calendar .pignose-calendar-unit a{
            padding: 0;
        }
        .side_nav ul li a.pignose-calendar-top-nav{
            background: none;
        }
        .pignose-calendar.pignose-calendar-blue .pignose-calendar-body .pignose-calendar-row .pignose-calendar-unit.pignose-calendar-unit-range a{
            background: #8fd2f7;
        }
    </style>
@endsection

@section('content')
    <!-- NOTIFICATION START-->
    <div class="notification d-none">
        <div class="notification_content">
            <h3>
                Your Layout has been published!
                <br>
                <a href="{{ route('publish') }}"><i class="fas fa-check-circle"></i></a>
            </h3>
        </div>
    </div>
    <!-- NOTIFICATION END-->

    <!-- SIDEBAR START -->
    <div class="side_nav d-none">
        <h5>Choose a Screen</h5>
        <ul class="screen">

            @if(count($displays)>0)
                @foreach($displays as $display)

                    <li>
                        <a href="#" class="d-flex entrance display" id="display-{{ $display->displayId }}"
                           data-displaygroupid="{{ $display->displayGroupId }}">
                            <i class="fa fa-desktop" aria-hidden="true"></i>
                            <span><b>{{ $display->display }}</b><br>{{ $display->timeZone }}</span>
                        </a>

                        <div class="d-none entrance-block">
                            <table class="pow">
                                <tr>
                                    <td><img src="{{ asset('img/potw.jpg') }}" alt="" class="img-fluid"></td>
                                    <td>Product of the week</td>
                                </tr>
                            </table>

                            <hr/>

                            <p><i class="far fa-calendar-alt"></i> Select Date Range</p>

                            <div id="v-cal">
                                <div class="calendar"></div>
                            </div>

                            <div class="pt-2"><input type="checkbox"> Constantly show</div>

                            <hr/>

                            <p><i class="far fa-calendar-alt"></i> Weekly</p>

                            <div class="d-flex justify-content-around text-center">
                                <span>Mon <br> <input type="checkbox"></span>
                                <span>Tue <br> <input type="checkbox"></span>
                                <span>Wed <br> <input type="checkbox"></span>
                                <span>Thu <br> <input type="checkbox"></span>
                                <span>Fri <br> <input type="checkbox"></span>
                                <span>Sat <br> <input type="checkbox"></span>
                                <span>Sun <br> <input type="checkbox"></span>
                            </div>
                            <div class="pl-2 pt-2"><input type="checkbox"> Whole Week</div>

                            <hr/>

                            <p><i class="far fa-clock"></i> Time</p>
                            <div class="pb-2">Show Between</div>
                            <div class="d-flex justify-content-around text-center">
                                <input type="text" placeholder="00:00:00" style="width: 100px;"
                                       class="start-timepicker">
                                <div class="p-2"></div>
                                <input type="text" placeholder="00:00:00" style="width: 100px;" class="end-timepicker">
                            </div>
                            <div class="pt-2"><input type="checkbox"> Whole Day</div>
                            <div class="text-center pt-4 d-flex justify-content-around">
                                {{--                                <a href="{{ route('home') }}" class="btn btn-primary btn-vsm">Menu</a>--}}
                                <a href="#" class="btn btn-primary btn-vsm btn-cancel">Back</a>
                                <a href="#" class="btn btn-primary btn-vsm btn-save">Save</a>
                            </div>
                        </div>
                    </li>

                @endforeach
            @endif
        </ul>
        <p class="side_nav_action">
            <a href="#" class="btn btn-primary btn-cancel">Cancel</a>
        </p>
    </div>
    <!-- SIDEBAR END -->

    <a href="#" class="help"><i class="far fa-question-circle"></i></a>

    <div class="container-fluid p-0">
        <section class="headerSec">
            <h2 class="tlt" data-in-effect="rollIn">Choose a Layout to Publish</h2>
        </section>
        <section class="p-3 p-lg-5 d-flex align-items-center choose-layout">
            <div class="w-100 text-center">
                <div class="container">
                    <div class="row">
                        @if(count($libraries)>0)
                            @foreach($libraries as $item)
                                <div class="col-md-4 pt-2">
                                    <input type="radio" id="layout{{$item->mediaId}}" name="layout" class="d-none media-item" data-mediaId="{{ $item->mediaId }}"/>
                                    <label for="layout{{ $item->mediaId }}">
                                        <div class="d-flex m-2">
                                            @if($item->type=='video')
                                                <i class="fas fa-film mr-3 h2"></i>
                                                <img src="{{ asset('img/potw.jpg') }}" alt="" class="img-fluid layouts">
                                            @elseif($item->type=='image')
                                                <i class="far fa-image mr-3 h2"></i>
                                                <img src="{{ asset('storage/files/'.$item->fileName) }}" alt=""
                                                     class="img-fluid layouts">
                                            @endif
                                        </div>
                                        <h5 class="text-center pl-5 mt-3">{{ $item->name }}</h5>
                                    </label>
                                </div>
                            @endforeach
                        @else
                            <div class="col-12 text-center">
                                No contents
                            </div>
                        @endif
                    </div>
                    <div class="text-center mt-5">
                        <a href="{{ route('publish') }}" class="btn btn-primary"><i class="fas fa-arrow-left"></i> Back</a>
                    </div>
                </div>
            </div>
            <form id="schedule_form" action="{{ route('schedule_media') }}" method="post">
                @csrf
                <input type='hidden' name='media_id' id="media_id" required>
                <input type='hidden' name='display_group_id' id="display_group_id" required>
                <input type='hidden' name='start_date' id="start_date" required>
                <input type='hidden' name='end_date' id="end_date">
                <input type='hidden' name='start_time' id="start_time" required>
                <input type='hidden' name='end_time' id="end_time" required>

            </form>
        </section>


    </div>
@endsection

@section('js')
    {{--    Clock scripts for this template--}}
    <script type="text/javascript" src="{{ asset('plugins/pg-calendar/js/pignose.calendar.full.min.js') }}"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
    {{--    Canlendar scripts for this template --}}
    {{--    <script src="{{ asset('js/vanillaCalendar.js') }}"></script>--}}
    <script type="text/javascript">
        $(function () {
            $('.calendar').pignoseCalendar({
                theme: 'blue',
                multiple:  true,
                select: function(date, obj) {
                    console.log((date[0] === null? 'null':date[0].format('YYYY-MM-DD')) +'~' +(date[1] === null? 'null':date[1].format('YYYY-MM-DD')));
                    if(date[0] !== null){
                        $('#start_date').val(date[0].format('YYYY-MM-DD'));
                    }else{
                        $('#start_date').val('');
                    }

                    if(date[1] !== null){
                        $('#end_date').val(date[1].format('YYYY-MM-DD'));
                    }else{
                        $('#end_date').val('');
                    }

                }
            });
        });


        jQuery('.start-timepicker').timepicker({
            change: function (e) {
                console.log(e.target.value);
                $('#start_time').val(e.target.value);
            },
            select: function (e, type) {
                $('#start_time').val(e.target.value);
            }
        });
        jQuery('.end-timepicker').timepicker({
            change: function (e) {
                console.log(e.target.value);
                $('#end_time').val(e.target.value);
            },
            select: function (e, type) {
                $('#end_time').val(e.target.value);
            }
        });

        // window.addEventListener('load', function () {
        //     vanillaCalendar.init({
        //         disablePastDays: true
        //     });
        // })

        $('.media-item').on('click', function (e) {
            const mediaId = $(this).data('mediaid');
            $('#media_id').val(mediaId);
        })

        $('.display').on('click', function () {
            const displayGroupId = $(this).data('displaygroupid');
            $('#display_group_id').val(displayGroupId);
        })

    //    Submit Schedule
        jQuery('.entrance-block .btn-save').click(function(e) {
            e.preventDefault();
            $('#schedule_form').submit();
            jQuery('.side_nav').addClass('d-none');
            jQuery('.notification').removeClass('d-none');
        });


    </script>
@endsection
