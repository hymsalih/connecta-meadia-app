@extends('layouts.app')
@section('css')
    <style>
        body {
            padding-top: 0;
            padding-right: 17rem;
        }
    </style>
@endsection

@section('content')
<!-- NOTIFICATION START-->
<div class="notification notification-scheduled d-none">
    <div class="notification_content">
        <h4 class="text-primary">DELETE THIS LAYOUT FROM SCHEDULE?</h4>
        <div class="d-flex justify-content-around pt-3 text-center">
            <a href="#" class="delete-scheduled">YES<br><i class="fas fa-check-circle h4 text-success"></i></a>
            <a href="#" class="delete-scheduled">NO<br><i class="fas fa-times-circle h4 text-danger"></i></a>
        </div>
    </div>
</div>
<div class="notification notification-running d-none">
    <div class="notification_content">
        <h4 class="text-primary">DELETING THIS LAYOUT WILL MEAN THAT NOTHING IS SHOWING ON YOUR SCREEN.<br/>DO YOU WANT
            TO PROCEED</h4>
        <div class="d-flex justify-content-around pt-3 text-center">
            <a href="#" class="delete-running">YES<br><i class="fas fa-check-circle h4 text-success"></i></a>
            <a href="#" class="delete-running">NO<br><i class="fas fa-times-circle h4 text-danger"></i></a>
        </div>
    </div>
</div>
<!-- NOTIFICATION END -->

<!-- SIDEBAR START -->
<nav class="navbar navbar-expand-lg navbar-dark bg-purple fixed-top" id="sideNav">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#"><i class="fa fa-desktop"></i> ENTRANCE<br><span>Landscape 50"</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="screens.html"><i class="fa fa-desktop"
                                                                             aria-hidden="true"></i> BREAD<br><span>Landscape 50"</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="shared.html"><i class="fa fa-desktop"></i> MEATS<br><span>Landscape 50"</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="shared.html"><i class="fa fa-desktop"></i> DAIRIES<br><span>Landscape 50"</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="shared.html"><i class="fa fa-desktop"></i> CASHIER<br><span>Landscape 50"</span></a>
            </li>
        </ul>

    </div>
    <p class="side_nav_action">
        <a href="{{ route('screens') }}" class="btn btn-primary">Back</a>
    </p>
</nav>
<!-- SIDEBAR END -->

<a href="#" class="help"><i class="far fa-question-circle"></i></a>
<div class="container-fluid p-0">
    <section class="resume-section p-3 p-lg-5">
        <div class="w-100 ">
            <h3 class="text-primary">
                {{ $resolution->resolution }}
            </h3>
            <p class="text-primary">{{ $resolution->width }} × {{ $resolution->height }}</p>
            <hr class="pb-3">
            <div class="container-fluid">
                <div class="row my-5">
                    <div class="col-md-3 border-right border-dark">
                        <img src="img/potw.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-3 border-right border-dark">
                        Standard Promotions
                    </div>
                    <div class="col-md-4 border-right border-dark">
                        <i class="fas fa-circle text-success"></i>
                        Running on schedule
                    </div>
                    <div class="col-md-2 text-center">
                        <a href="{{ route('template_create') }}" class="btn btn-success btn-sm">Edit</a>
                        <a href="#" class="btn btn-danger btn-sm mt-5 btn-running-delete">Delete</a>
                    </div>
                </div>
                <div class="row my-5 inactive">
                    <div class="col-md-3 border-right border-dark">
                        <img src="img/potw.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-3 border-right border-dark">
                        Campaign Promotions
                    </div>
                    <div class="col-md-4 border-right border-dark">
                        <i class="fas fa-circle text-danger"></i>
                        Scheduled for start 19-05-14 10:00
                    </div>
                    <div class="col-md-2 text-center">
                        <a href="{{ route('template_create') }}" class="btn btn-success btn-sm">Edit</a>
                        <a href="#" class="btn btn-danger btn-sm mt-5 btn-scheduled-delete">Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


</div>
@endsection
