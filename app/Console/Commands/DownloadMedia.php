<?php

namespace App\Console\Commands;

use App\Media;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DownloadMedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'media:download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download media from media library';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $provider = new \Xibo\OAuth2\Client\Provider\Xibo([
            'clientId' => env('CLIENT_ID'),    // The client ID assigned to you by the provider
            'clientSecret' => env('CLIENT_SECRET'),   // The client password assigned to you by the provider
            'redirectUri' => '',
            'baseUrl' => env('API_BASE_URL')
        ]);

        $token = $provider->getAccessToken('client_credentials')->getToken();

        $entityProvider = new \Xibo\OAuth2\Client\Provider\XiboEntityProvider($provider);

        Log::debug(storage_path('app/public/files/'));
        $media = Media::where('isLocal', 0)->get();
        foreach ($media as $item){
            $result = (new \Xibo\OAuth2\Client\Entity\XiboLibrary($entityProvider))->download($item->mediaId, $item->type, storage_path('app/public/files/') , $item->fileName);
            if($result){
                error_log($item->id);
                $row = Media::find($item->id);
                $row->isLocal = 1;
                $row->save();
            }
        }


    }
}
