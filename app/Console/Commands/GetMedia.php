<?php

namespace App\Console\Commands;

use App\Media;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GetMedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'media:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get media from Media Library';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $provider = new \Xibo\OAuth2\Client\Provider\Xibo([
            'clientId' => env('CLIENT_ID'),    // The client ID assigned to you by the provider
            'clientSecret' => env('CLIENT_SECRET'),   // The client password assigned to you by the provider
            'redirectUri' => '',
            'baseUrl' => env('API_BASE_URL')
        ]);

        $token = $provider->getAccessToken('client_credentials')->getToken();
        Log::debug($token);

        $entityProvider = new \Xibo\OAuth2\Client\Provider\XiboEntityProvider($provider);

        $users = User::all();
        foreach ($users as $user) {
            $library = (new \Xibo\OAuth2\Client\Entity\XiboLibrary($entityProvider))->get(['tags' => $user->name, 'start' => 0, 'length' => 1000]);
            foreach ($library as $item) {
                $result = Media::where('mediaId', $item->mediaId)->get();
                \Log::debug($item->mediaType);
                if ($item->mediaType != 'image' && $item->mediaType != 'video')
                    continue;
                if (count($result) == 0) {
                    $newItem = new Media;
                    $newItem->mediaId = $item->mediaId;
                    $newItem->name = $item->name;
                    $newItem->type = $item->mediaType;
                    $newItem->tag = $item->tags;
                    $newItem->ownerId = $item->ownerId;
                    $newItem->fileName = $item->fileName;
                    $newItem->created_at = $item->createdDt;
                    if (substr($item->modifiedDt, 1) == '-')
                        $newItem->updated_at = $item->modifiedDt;
                    else
                        $newItem->updated_at = null;
                    $newItem->save();
                } else {
                    $updateItem = Media::where('mediaId', $item->mediaId)->first();
                    $updateItem->name = $item->name;
                    $updateItem->type = $item->mediaType;
                    $updateItem->tag = $item->tags;
                    $updateItem->ownerId = $item->ownerId;
                    $updateItem->fileName = $item->fileName;
                    $updateItem->created_at = $item->createdDt;
                    if (substr($item->modifiedDt, 1) == '-')
                        $updateItem->updated_at = $item->modifiedDt;
                    else
                        $updateItem->updated_at = null;
                    $updateItem->save();

                }
//            if ($item->mediaType == 'image')
//                $media = (new \Xibo\OAuth2\Client\Entity\XiboLibrary($entityProvider))->download($item->mediaId, $item->mediaType, storage_path('app/public/files/') , $item->fileName);

            }
        }

    }
}
