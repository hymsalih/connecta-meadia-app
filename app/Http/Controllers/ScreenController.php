<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ScreenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function screens(){

        $provider = new \Xibo\OAuth2\Client\Provider\Xibo([
            'clientId' => env('CLIENT_ID'),    // The client ID assigned to you by the provider
            'clientSecret' => env('CLIENT_SECRET'),   // The client password assigned to you by the provider
            'redirectUri' => '',
            'baseUrl' => env('API_BASE_URL')
        ]);

//        $token = $provider->getAccessToken('client_credentials')->getToken();
        $entityProvider = new \Xibo\OAuth2\Client\Provider\XiboEntityProvider($provider);
        $resolutions = (new \Xibo\OAuth2\Client\Entity\XiboResolution($entityProvider))->get();
        return view('screens')->with([
            'resolutions' => $resolutions
        ]);
    }

    public function screen($resolutionId){
        $provider = new \Xibo\OAuth2\Client\Provider\Xibo([
            'clientId' => env('CLIENT_ID'),    // The client ID assigned to you by the provider
            'clientSecret' => env('CLIENT_SECRET'),   // The client password assigned to you by the provider
            'redirectUri' => '',
            'baseUrl' => env('API_BASE_URL')
        ]);

//        $token = $provider->getAccessToken('client_credentials')->getToken();
        $entityProvider = new \Xibo\OAuth2\Client\Provider\XiboEntityProvider($provider);
        $resolution = (new \Xibo\OAuth2\Client\Entity\XiboResolution($entityProvider))->getById($resolutionId);
        return view('screen')->with([
            'resolution' => $resolution
        ]);
    }
}
