<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function schedule_media(Request $request){
        $provider = new \Xibo\OAuth2\Client\Provider\Xibo([
            'clientId' => env('CLIENT_ID'),    // The client ID assigned to you by the provider
            'clientSecret' => env('CLIENT_SECRET'),   // The client password assigned to you by the provider
            'redirectUri' => '',
            'baseUrl' => env('API_BASE_URL')
        ]);

        $media_id = $request->media_id;
        $displayGroupId = $request->display_group_id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $start_time = $request->start_time;
        $end_time = $request->end_time;
        $start_date_time = $start_date.' '.$start_time.':00';
        $end_date_time = $end_date.' '.$end_time.':00';

        $token = $provider->getAccessToken('client_credentials')->getToken();
        Log::debug($token);

        $entityProvider = new \Xibo\OAuth2\Client\Provider\XiboEntityProvider($provider);

        \Log::debug(Auth::user()->id);
        \Log::debug(Auth::user()->userId);
//        $resolutions = (new \Xibo\OAuth2\Client\Entity\XiboResolution($entityProvider))->get();
//        $displays = (new \Xibo\OAuth2\Client\Entity\XiboDisplay($entityProvider))->get(['tags' => Auth::user()->name]);
        $layout = (new \Xibo\OAuth2\Client\Entity\XiboLayout($entityProvider))->create(Auth::user()->name.'_test'.rand(1, 1000000), Auth::user()->name.'_test', '', 1);

        $layoutDraft = (new \Xibo\OAuth2\Client\Entity\XiboLayout($entityProvider))->getById($layout->layoutId, 'regions,playlists,widgets');
        $layoutDraft1 = (new \Xibo\OAuth2\Client\Entity\XiboLayout($entityProvider))->checkout($layoutDraft->layoutId);
        $layoutDraft1 = (new \Xibo\OAuth2\Client\Entity\XiboRegion($entityProvider))->checkout($layoutDraft->layoutId);
        $widgetList = (new \Xibo\OAuth2\Client\Entity\XiboWidget($entityProvider))->get(['playlistId' => 136]);
        $regions = $layoutDraft1->regions;
        $region = $regions[0];
        $region_playlistId = $region->regionPlaylist->playlistId;
        $media = [$media_id];
        Log::debug('draft');
        Log::debug($layoutDraft1->layoutId);
        Log::debug($layoutDraft->layoutId);
        $clock = (new \Xibo\OAuth2\Client\Entity\XiboClock($entityProvider))->create('Api Analogue clock', 20, 1, 1, 1, NULL, NULL, NULL, NULL, $region->playlists[0]['playlistId']);
        $playlist = (new \Xibo\OAuth2\Client\Entity\XiboPlaylist($entityProvider))->assign($media, 10, $region_playlistId);
        $playlist = (new \Xibo\OAuth2\Client\Entity\XiboPlaylist($entityProvider))->add('BoopDynamic', '', 1, '', 'moonmoon');
        $layout1 = $layoutDraft1->publish($layoutDraft1->layoutId);
        Log::debug($layout1->layoutId);

//        $clock = (new \Xibo\OAuth2\Client\Entity\XiboClock($entityProvider))->create('Api Analogue clock', 20, 1, 1, 1, NULL, NULL, NULL, NULL, $region->playlists[0]['playlistId']);
//        $text = (new \Xibo\OAuth2\Client\Entity\XiboText($entityProvider))->create('Text item', 10, 1, 'marqueeRight', 5, null, null, 'TEST API TEXT', null, $region->playlists[0]['playlistId']);
//        $audio = (new \Xibo\OAuth2\Client\Entity\XiboAudio($entityProvider))->assignToWidget($media_id, 50, 1, $clock->widgetId);

//        $layout->publish($layout->layoutId);
        $event = (new \Xibo\OAuth2\Client\Entity\XiboSchedule($entityProvider))->createEventLayout(
            $start_date_time,
            $end_date_time,
            $layout->campaignId,
            [$displayGroupId],
            4,
            NULL,
            NULL,
            NULL,
            0,
            0,
            0
        );
        return back();
    }

    public function publish(){
        return view('publish');
//        $provider = new \Xibo\OAuth2\Client\Provider\Xibo([
//            'clientId' => env('CLIENT_ID'),    // The client ID assigned to you by the provider
//            'clientSecret' => env('CLIENT_SECRET'),   // The client password assigned to you by the provider
//            'redirectUri' => '',
//            'baseUrl' => env('API_BASE_URL')
//        ]);
//
//
//        $token = $provider->getAccessToken('client_credentials')->getToken();
//        Log::debug($token);
//
//        $entityProvider = new \Xibo\OAuth2\Client\Provider\XiboEntityProvider($provider);
//
//        $layoutDraft = (new \Xibo\OAuth2\Client\Entity\XiboLayout($entityProvider))->getById(2021, 'regions,playlists,widgets');
////        $layoutDraft1= $layoutDraft->checkout($layoutDraft->layoutId);
//        $layoutDraft->publish(2021);
////        $layoutDraft->publish($layoutDraft->layoutId);
////        $layoutDraft1 = (new \Xibo\OAuth2\Client\Entity\XiboLayout($entityProvider))->getById(2021, 'regions,playlists,widgets');
//        dd($layoutDraft);
    }
}
