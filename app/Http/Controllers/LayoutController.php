<?php

namespace App\Http\Controllers;

use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Auth;

class LayoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function layouts()
    {
        $provider = new \Xibo\OAuth2\Client\Provider\Xibo([
            'clientId' => env('CLIENT_ID'),    // The client ID assigned to you by the provider
            'clientSecret' => env('CLIENT_SECRET'),   // The client password assigned to you by the provider
            'redirectUri' => '',
            'baseUrl' => env('API_BASE_URL')
        ]);


        $token = $provider->getAccessToken('client_credentials')->getToken();
        Log::debug($token);

        $entityProvider = new \Xibo\OAuth2\Client\Provider\XiboEntityProvider($provider);

        \Log::debug(Auth::user()->id);
        \Log::debug(Auth::user()->userId);
//        $resolutions = (new \Xibo\OAuth2\Client\Entity\XiboResolution($entityProvider))->get();
        $displays = (new \Xibo\OAuth2\Client\Entity\XiboDisplay($entityProvider))->get(['tags' => Auth::user()->name]);

        $library = Media::where('tag', 'like', '%' . Auth::user()->name . '%')->get();
//        foreach ($library as $item) {
//            if($item->mediaType=='image')
//            $media = (new \Xibo\OAuth2\Client\Entity\XiboLibrary($entityProvider))->download($item->mediaId, $item->mediaType, 'files/' , $item->fileName);
//        }

        if (Auth::user()->userId == null) {
            return view('layouts')->with([
                'displays' => $displays,
                'libraries' => $library
            ]);
        }


        $layouts = (new \Xibo\OAuth2\Client\Entity\XiboLayout($entityProvider))->getById(Auth::user()->userId);

        if (gettype($layouts) == 'object') {
            return view('layouts')->with([
                'displays' => $displays,
                'libraries' => $library
            ]);
        }

        return view('layouts')->with([
            'displays' => $displays,
            'libraries' => $library
        ]);
    }
}
