<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'userId' => $data['userId']
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $provider = new \Xibo\OAuth2\Client\Provider\Xibo([
            'clientId' => env('CLIENT_ID'),    // The client ID assigned to you by the provider
            'clientSecret' => env('CLIENT_SECRET'),   // The client password assigned to you by the provider
            'redirectUri' => '',
            'baseUrl' => env('API_BASE_URL')
        ]);

        $token = $provider->getAccessToken('client_credentials')->getToken();
        # Create Xibo Entity Provider with logger
        $entityProvider = new \Xibo\OAuth2\Client\Provider\XiboEntityProvider($provider);

        try {
            $remoteUser = (new \Xibo\OAuth2\Client\Entity\XiboUser($entityProvider))->create($request->name, 3, 6, $request->password, 83, 1, 0, $request->email);
            $this->validator($request->all())->validate();

            $userData = $request->all();
            $userData['userId'] = $remoteUser->userId;
            event(new Registered($user = $this->create($userData)));

            $this->guard()->login($user);

            return $this->registered($request, $user)
                ?: redirect($this->redirectPath());
        } catch (\Exception $e) {
            \Log::debug($e);
            return redirect('register');
        }

    }

}
