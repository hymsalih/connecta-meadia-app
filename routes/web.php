<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    Route::get('/layouts', 'LayoutController@layouts')->name('layouts');
    Route::get('/templates', 'HomeController@templates')->name('templates');
    Route::get('/template_create', 'TemplateController@template_create')->name('template_create');
    Route::get('/publish', 'HomeController@publish')->name('publish');
    Route::get('/screens', 'ScreenController@screens')->name('screens');
    Route::get('/screen/{resolutionId}', 'ScreenController@screen')->name('screen');
    Route::get('/shared_media', 'HomeController@shared_media')->name('shared_media');
    Route::post('/schedule_media', 'ScheduleController@schedule_media')->name('schedule_media');
    Route::get('/publish', 'ScheduleController@publish')->name('publish');

});
